import json,urllib

def ebay_price_grabber( min_price1, max_price1,searchTerm ):

    '''This function takes the max and min prices that a user may wish to search for, it alos takes a searchterm to search for products '''
    
    import re
    
    #min_price1 = "100.00"
    #max_price1 = "150.00"
    min_price = float(min_price1)
    max_price = float(max_price1)
    searchTerm=str(searchTerm)
    print " start "
    

    try:
       data = urllib.urlopen("http://pipes.yahoo.com/pipes/pipe.run?_id=e3d648bd8fdb6ece8ee8d41a2a2a3aa3&_render=json").read()
       
    except "Data not fetched successfully!":
       print "Error: can\'t find data sources"
    
    d = json.loads(data)

    if d < 0:
        print " empty"
        
    list1 = []
    list2 = []
    list3 = []
    list4 = []
    list5 = []  # this stores the matched items
    list6 = []
    list7 = []
    list8 = []
    
    for q in d['value']['items']:
        current_price = q['title']
        f_price = float (getPrice(current_price))
        if (f_price < max_price) & (f_price > min_price):
            list4.append(f_price)
            list1.append(q['title'])
            list2.append(q['link'])
            list3.append(q['summary']['div']['img']['src'])
            print "\n", f_price
        
        else:
            print "No items found below your requirements"
            
    
    zipped = zip(list1,list2,list3)
    
    for x in zipped:
        #print "\n", x
        #print x[0]

        #searchTerm
        searchTag = re.compile(searchTerm,re.IGNORECASE) # we should take this input from the user


        content = x[0]
        temp = []
        temp = re.findall(searchTag,content)
        if len(temp) < 0 : #This statement tests if a matching string was found in the website content
            print "Search Tag not found"
            #list6.append(r['title'])
            #list7.append(r['link'])
            #list8.append(r['summary']['div']['img']['src'])
            #s=1
        elif len(temp) > 0:
            print "Search Tag found in searchWord() function"
            list6.append(x[0])
            list7.append(x[1])
            list8.append(x[2])

    print " end for"
    
    zipped2 = zip(list6,list7,list8)

     
    for i in zipped2:
       print i
    
       s=1
   
    return zipped2 # this should be sent to the event/response function for output      
    
####################################################################################    

def getPrice(s):
    ''' This function parses the prices from the data gathered from Craigslist and Ebay'''
    res =  re.search(r'\d+\.\d+', s)
    if res is None:
        return 0.
    else:
        return float(res.group(0))

    prices = map(getPrice, res)
    return prices



