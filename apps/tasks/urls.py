from django.conf.urls.defaults import *
from django.views.generic.simple import direct_to_template


urlpatterns = patterns("",
    #url(r"^$", direct_to_template, {"template": "about/about.html"}, name="about"),
    url(r"^channel/$", direct_to_template, {"template": "tasks/channel.html"}, name="channel"),
    url(r"^mixes/$", direct_to_template, {"template": "tasks/mixes.html"}, name="mixes"),
    url(r"^invite/$", direct_to_template, {"template": "tasks/invite.html"}, name="invite"),
    url(r"^viewtask/$", direct_to_template, {"template": "tasks/viewtask.html"}, name="viewtask"),	

)
