==============
Introductiion
==============

Overview
--------------

Many years ago, now-defunct “Sun Microsystems” had the slogan “The network *IS* the 
computer.” We may view the information on the web as a large storage system similar
to a database. 

As information in the Internet changes we can imagine that observable changes generate events. If we want to “script the Internet” then we want to initiate actions based on these events. 

For example, if we are very concerned about extra-terrestrial invasion, we may want to receive an SMS if any top stories of the New York Times, or Washington Post have the word “extra-terrestrial” in the title. As another example, I may want to receive an email if the price of an item drops below a threshold. If a tennis court becomes available between within some time range, we may a script to reserve a court and put the time on my calendar, and a friends. 

Currently there are some similar technologies for creating a web “workflow.” There is Yahoo Pipes (http://pipes.yahoo.com/pipes/), If this then that (http://ifttt.com/), and Wappwolf (http://wappwolf.com/). All of these are cool services. What would be great is to create an open source Django or Pyramid plugin to do the
same thing so anyone could host this kind of service. This project will first build a web API that give the functionality of something like ifttt, then a web front end. 


Mission
----------------
Create an open source application that uses the same functionality of ifttt and expanding by implementing other apis such as yahoo pipes.
