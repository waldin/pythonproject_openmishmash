from django import forms
from uni_form.helpers import FormHelper
from uni_form.layout import Layout,Fieldset

class   Auth(forms.Form):
	email=forms.EmailField(required=True)

class SearchGmail(forms.Form):
	email=forms.EmailField(required=True)
        subject=forms.CharField(max_length=100)

class SearchGmail_body(forms.Form):
        search=forms.CharField(max_length=50)

class PriceWatcher(forms.Form):
      item=forms.CharField(max_length=150)
      max_price=forms.IntegerField()
      min_price=forms.IntegerField()

class SendMail(forms.Form):
        to=forms.EmailField(required=True)
        subject=forms.CharField(max_length=100)
        body=forms.CharField(widget=forms.Textarea)

class SendTweet(forms.Form):
       tweet=forms.CharField(max_length=140)

class MixComplete(forms.Form):
       description=forms.CharField(widget=forms.Textarea(attrs={'rows':2,'cols':50}))

