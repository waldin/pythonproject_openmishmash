from forms import *
from models import *
from sign_in_with_google.google import views
from twitter import views as tviews
from scraper import ebay

# Takes step values and returns current process
def get_category(step):
       '''
       Helper function to determine current process trigger or event
       Accepts a numerical string between 1 and 6
       '''       
       try:
          step=int(step)
       except ValueError:
          return None

       # 1 to 3 indicates trigger creation process
       if 1<=step<=3:
           return "trigger"
       # 4 to 6 indicates event creation process
       elif 4<=step<=6:
           return "event"
       else:
           return None

# Renders proper form for provided service
def service_form(service_name):
    if service_name=="BySender":
       form=SearchGmail()
    elif service_name=="SearchTerm":
       form=SearchGmail_body()
    elif service_name=="EbayPriceWatcher" or service_name=="CraigslistPriceWatcher":
       data={'item':'place item name here','max_price':0,'min_price':0}
       form=PriceWatcher(data)
    elif service_name=="SendEmail":
       form=SendMail()
    elif service_name=="SendTweet":
       form=SendTweet()
    return form


# Places input returned from a service form into the proper form format
def service_input(service_name,d):
    if service_name=="BySender":
       form=SearchGmail(d)
    elif service_name=="SearchTerm":
       form=SearchGmail_body(d)
    elif service_name=="EbayPriceWatcher":
       form=PriceWatcher(d)
    elif service_name=="CraigslistPriceWatcher":
       form=PriceWatcher(d)
    elif service_name=="SendEmail":
       form=SendMail(d)
    elif service_name=="SendTweet":
       form=SendTweet(d)
    return form

# Calls function that performs provided service
def service_action(service_name,user_d,params):
    if service_name=="BySender":
       content=views.search_Gmail(user_d,params['subject'],params['email'])
       return content
    elif service_name=="SearchTerm":
       content=views.search_gmail_body(user_d,params['search'])
       return content
    elif service_name=="EbayPriceWatcher":
       content=ebay.ebay_price_grabber(params['minprice'],params['maxprice'],params['item'])
       return content
    elif service_name=="SendEmail":
       views.sendGmail(user_d,params['to'],params['subject'],params['body'])
    elif service_name=="SendTweet":
       tviews.twitter_sendtweet(user_d,params['tweet'])

# Gets authorisation data for specified user and channel
def get_details(user,channel):
    access=Access.objects.get(user=user,channel=channel)
    token=access.access_token.split('::')
    key=token[0]
    secret=token[1]
    details={'uid':access.uid,'key':key,'secret':secret}
    return details

#Function that constantly runs to perform the task of all create mixes
def runner():
    users=User.objects.filter(is_superuser==False)
    
    for user in users:
        ownership=Owner.objects.filter(user=user)
        for owned in ownership:
            mix=owned.mix
            processor(user,mix)

# Processes the given user mix and runs each task
def processor(user,mix):
       owner=Owner.objects.get(user=user,mix=mix)
       criteria=Owner_criteria.objects.filter(owner=owner)
       t_params={}
       e_params={}
       for c in criteria:
         if c.process=="trigger":
            t_params[c.key]=c.val
         else:
            e_params[c.key]=c.val
            
       trigger={}
       event={}

       

       t_service=mix.trigger
       e_service=mix.event


       t_channel=t_service.channel
       e_channel=e_service.channel
       
       if t_channel.id!=e_channel.id:
         if t_channel.access_req==True:
            trigger['access']=get_details(user,t_channel)
         else:
            trigger['access']=True

         event['access']=get_details(user,e_channel) 
          
       else:
         detail=get_details(user,t_channel)
         trigger['access']=detail
         event['access']=detail

       trigger['service_name']=t_service.tag
       trigger['params']=t_params
       
       event['service_name']=e_service.tag
       event['params']=e_params
       
       fired=False
       content=service_action(trigger['service_name'],trigger['access'],trigger['params'])
       if content:
          service_action(event['service_name'],event['access'],event['params'])
          fired=True
       
       return fired
