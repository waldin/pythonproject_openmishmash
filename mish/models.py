from django.db import models
from django.contrib.auth.models import User

	
class Channel(models.Model):
	name=models.CharField(max_length=50)
	consumer=models.CharField(max_length=100)
	consumer_key=models.CharField(max_length=100)
        access_req=models.BooleanField()
	def __unicode__(self):
        	return self.name

class Access(models.Model):
	channel=models.ForeignKey(Channel)
	user=models.ForeignKey(User)
	uid=models.CharField(max_length=150)
	access_token=models.CharField(max_length=150)

class Meta:
	unique_together=('user','channel')

class Service(models.Model):
	channel=models.ForeignKey(Channel)
	tag=models.CharField(max_length=20)
	category=models.CharField(max_length=20)
	description=models.CharField(max_length=100)

	def __unicode__(self):
        	return u'%s %s' %(self.channel.name,self.tag)

class Meta:
	unique_together=('channel','tag')


class Mix(models.Model):
	creator=models.ForeignKey(User)
	trigger=models.ForeignKey(Service,related_name='mix_trigger')
	event=models.ForeignKey(Service,related_name='mix_event')
	desc=models.CharField(max_length=140)
	shared=models.BooleanField()

	def __unicode__(self):
        	return u'%s %s %s %s' % (self.trigger.channel.name,self.trigger.tag,self.trigger.channel.name,self.event.tag)

class Meta:
	unique_together=('creator','trigger','event')

class Owner(models.Model):
	user=models.ForeignKey(User)
	mix=models.ForeignKey(Mix)
	active=models.BooleanField()

	def __unicode__(self):
        	return u'%s %s' % (self.user.username,self.mix.desc)

class Meta:
	unique_together=('user','mix')

class Owner_criteria(models.Model):
	owner=models.ForeignKey(Owner)
	process=models.CharField(max_length=20)
        key=models.CharField(max_length=20)
	val=models.CharField(max_length=250)

	def __unicode__(self):
        	return u'%s %s' % (self.user.username,self.mix.desc)


