from django import template
from django.template.defaultfilters import stringfilter
import re

register=template.Library()

@register.filter
@stringfilter

def spacer(value):
    if value!='':
       term=re.findall('[A-Z][^A-Z]*', value)
       if term:
          complete=''
          for word in term:
              complete +=' ' +word

          return complete
       else:
          return value

