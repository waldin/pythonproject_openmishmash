"""
This file demonstrates writing tests using the unittest module. These will pass
when you run "manage.py test".

Replace this with more appropriate tests for your application.
"""
from django.test.client import Client
from django.test import TestCase
from django.http import HttpResponse, Http404, HttpResponseRedirect
from django.utils import unittest
from mash import views
import mixer


class MixerFunctionsTest(unittest.TestCase):
      knownprocesses=( ('1','trigger'),
                       ('2','trigger'),
                       ('3','trigger'),
                       ('4','event'),
                       ('5','event'),
                       ('6','event'),)
       
      def testStepToProcess(self):
          for step, process in self.knownprocesses:
            result=mixer.get_category(step)
            self.assertEqual(process,result)

      def testStepOutofRange(self):
          stepout=7
          result=mixer.get_category(stepout)
          self.assertEqual(None,result)

      def testNonNumerical(self):
          text="string"
          result=mixer.get_category(text)
          self.assertEqual(None,result)
        
              
          
      






	
