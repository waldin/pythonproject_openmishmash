from django.template.loader import get_template
from django.http import HttpResponse, Http404, HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.shortcuts import render_to_response, redirect, get_list_or_404, get_object_or_404
from django.template import RequestContext
from models import *
from forms import *
from sign_in_with_google.google import *
from twitter import *
import re
import mixer



#===============================================================================================================

def channels(request,step):
    '''#Outputs list of channels available to current process'''
       
    if step=="1" or step=="4":
       #calls helper function to determine current process
       cat=mixer.get_category(step)
    else:
       return Http404
    #Makes sure a user is logged in
    if request.user.is_authenticated()==False:
          return HttpResponseRedirect('/account/login')

    #increments step to next value in the process sequence
    try:
      curr_step=int(step)
      next=str(curr_step+1)
    except ValueError:
      raise Http404

    #return list of channels relevant to current step
    try:
      chanlist=Channel.objects.filter(service__category=cat).distinct()
    except ValueError:
      raise Http404

    #stores retrieved values in session variables
    request.session['next']=next
    request.session['channel_list']=chanlist
	  
    request.session.modified
    #sends data to template for output
    return render_to_response('mash/_channel_list.html',{'cat':cat},context_instance=RequestContext(request))
     
#===============================================================================================================

def activate(request):
    '''#Checks and created access for choosen channels'''

    #view is only called through post
    if request.method!='POST':
	raise Http404


    user=request.user
    
    #retrieves next step in process sequence from session
    step=request.session.get('next','')
    cat=mixer.get_category(step)
   
    #Determines which image button created the post request
    for key in request.POST:
	if re.search('.*.y',key) or re.search('.*.x',key):
	   submitted=key[:-2]
	   break

    #if submition was not from activation form...it was from a channel selection
    if submitted!="Activate":
       #get channel object
       channel_name=submitted 
       channel=get_object_or_404(Channel,name=channel_name)
       
       #store channel name and default access status in session variables (access originally assumed true)
       request.session['channel']=channel_name
       request.session['access_grant']=True

       #if channel doesnt need access or user has access to the channel object
       if channel.access_req==False or Access.objects.filter(channel=channel,user=user).exists():
          #call function for service selection with current process sequence as an arguement
          return HttpResponseRedirect(reverse('mash.views.services',args=step))
       # if user does not have access
       else:
          #change access status to fals
          request.session['access_grant']=False
          #retrieve authorization form and send to channel access template
          form=False
          if(channel_name=="Gmail"):
            form=Auth()
              
          return render_to_response('mash/channel_access.html',{'form':form},context_instance=RequestContext(request))
    #otherwise an authorization form was submitted
    else:
       #get channel name from request
       channel=request.POST['channel']
       
       #determine which channel was authorized for proper form format and authorization function call
       if(channel=="Gmail"):
          form=Auth(request.POST)
          if form.is_valid():
             auth=form.cleaned_data

             #add authorization parameter to session
             request.session['gmail']=auth['email']
	     request.session.modified=True

             return HttpResponseRedirect('/create/trigger/Gmail/google/')
       elif (channel=="Twitter"):
             
             return HttpResponseRedirect('/mix/create/auth/twitter/')
#============================================================================================================== 


def services(request,step):
    '''#Outputs all api services available to choosen channel and current process'''

    #Makes sure a user is logged in


    if request.user.is_authenticated()==False:
          return HttpResponseRedirect('/account/login')

    
    request.session['access_grant']=True
    channel_name=request.session.get('channel','')
    channel=get_object_or_404(Channel,name=channel_name)
    cat=mixer.get_category(step)
    try:
       curr_step=int(step)
       next=curr_step+1
       request.session['next']=next
    except ValueError:
       raise Http404  
    
    #Collects a list of services by filtering the channel id and process category
    service=Service.objects.filter(channel__id=channel.id,category=cat)              
    print service
    return render_to_response('mash/service_select.html',{'cat':cat,'service':service,'selecting':True},context_instance=RequestContext(request))
#================================================================================================================

def service_criteria(request,step):
    if request.method!='POST':
	raise Http404
    #Makes sure a user is logged in
    if request.user.is_authenticated()==False:
          return HttpResponseRedirect('/account/login')
    
    cat=mixer.get_category(step)
    channel_name=request.session.get('channel','')

    #If function was called from service selection
    if request.POST.get('serv_submit','')=='':
      #Loop through request.POST dictionary to get the name of choosen service
      for key in request.POST:
	if re.search('.*.y',key) or re.search('.*.x',key):
	   service_name=key[:-2]
	   break

      #Gets service object based on channel name and service tag
      service=Service.objects.get(channel__name=channel_name,tag=service_name)
      #Store object in session
      request.session['service']=service
      #Call mixer service_form function to retrieve form for the service
      form=mixer.service_form(service_name)
      return render_to_response('mash/service_select.html',{'form':form,'cat':cat,'serv':service}, context_instance=RequestContext(request))

    else:
      #A service criteria form has been submitted and the data must be collected
      #Get service object from session
      service=request.session['service']
      #Store data into corresponding form by calling service_input function
      form=mixer.service_input(service.tag,request.POST)
      if form.is_valid():
         #Store cleaned data into new dictionary
         params=form.cleaned_data
         #Create a dictionary that stores service and parameters into process sessions
         data={'service':service,'params':params}
         print 'got her'
         if mixer.get_category(request.session.get('next'))=="trigger":
            request.session['trigger']=data
            return HttpResponseRedirect('/mix/create/step4/')
         else:
     	    request.session['event']=data
            return HttpResponseRedirect('/mix/create/overview')
#===============================================================================================================
def complete(request):
    if request.user.is_authenticated()==False:
          return HttpResponseRedirect('/account/login')

    #Get process information from session variables
    trigger=request.session.get('trigger')
    event=request.session.get('event')

    #Store service objects into seperate variables
    t_service=trigger['service']
    e_service=event['service']

    #Get channel objects from the service objects
    t_channel=t_service.channel
    e_channel=e_service.channel

    #Store critera information into variables
    t_params=trigger['params']
    e_params=event['params']
   
    #Store channel and service information into one dictionary
    data={'t_channel':t_channel.name,'e_channel':e_channel.name,'t_serv':t_service.tag,'e_serv':e_service.tag}
    
    #If view was called by a post then a mix description was submitted
    if request.method=='POST':
       #Get user
       user=request.user
       #Create form to get mix description from user
       form=MixComplete(request.POST)
       if form.is_valid():
          complete_form=form.cleaned_data
          #store description in variable
          description=complete_form['description']
     
          #Create mix object and save  
          mix=Mix(creator=user,trigger=t_service,event=e_service,desc=description,shared=False)
          mix.save()
          #Create a new owner object with user and new mix object
          owner=Owner(user=user,mix=mix,active=True)
          owner.save()
           
          #Iterate through criteria dictionaries and seperate based on process trigger/event
          for field,value in t_params.iteritems():
              criteria=Owner_criteria(owner=owner,process="trigger",key=field,val=value)
              criteria.save()

          for field,value in e_params.iteritems():
              criteria=Owner_criteria(owner=owner,process="event",key=field,val=value)
              criteria.save()
         
          #Call function to perform mix task once after initail creation
          fired=mixer.processor(user,mix)

          return render_to_response('mash/complete.html',{'complete':True,'fired':fired},context_instance=RequestContext(request))           
    else:
          form=MixComplete()
          return render_to_response('mash/complete.html',{'data':data,'form':form},context_instance=RequestContext(request))



#========================================================================================================
#========================================================================================================

