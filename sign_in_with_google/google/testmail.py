from gmailConnector import *

def sendGmail(sender,recipient,subject,body): 
  ''' This function implements the Gmail Response to respond to any  '''
  import xoauth
  import smtplib
  import config
  import base64

  SMTP_SERVER = 'smtp.gmail.com'
  SMTP_PORT = 587
  
  auth = gmailAuthentication(sender)
  access_token = auth[0]
  consumer = auth[2]
  
          
  #global xoauth_string  # this actually is suppose be retrieved from the database in the future
  
  xoauth_string = xoauth.GenerateXOauthString(consumer,access_token,sender,'smtp',xoauth_requestor_id=None, nonce=None, timestamp=None)


  "Sends an e-mail to the specified recipient."
 
  body = "" + body + ""
 
  headers = ["From: " + sender,
                "Subject: " + subject,
                "To: " + recipient,
                "MIME-Version: 1.0",
                "Content-Type: text/html"]
  headers = "\r\n".join(headers)


  print
  smtp_conn = smtplib.SMTP(SMTP_SERVER, 587)
  smtp_conn.ehlo()
  smtp_conn.set_debuglevel(True)
  smtp_conn.ehlo()
  smtp_conn.starttls()
  smtp_conn.ehlo()
  smtp_conn.docmd('AUTH', 'XOAUTH ' + base64.b64encode(xoauth_string))
  smtp_conn.sendmail(sender, recipient, headers + "\r\n\r\n" + body) 

  smtp_conn.connect(SMTP_SERVER,SMTP_PORT)
  smtp_conn.ehlo()

  #xoauth.TestSmtpAuthentication(SMTP_SERVER, sender, xoauth_string)   # this sworks

################################SEARCHING THE INBOX USING OAUTH CREDENTIALS ######################################

def search_gmail_subject_personfrom(user,searchTag, personfrom):

    ''' This function searches a users inbox based on who the message came from and of a particular subject'''
    
    import imaplib
    import xoauth
    import email

    
    auth = gmailAuthentication(user)
    access_token = auth[0]
    consumer = auth[2]
   
         
    xoauth_string = xoauth.GenerateXOauthString(consumer,access_token,user,'imap',xoauth_requestor_id=None, nonce=None, timestamp=None)
  
    imap_hostname = 'imap.googlemail.com'
    imap_conn = imaplib.IMAP4_SSL(imap_hostname)
    imap_conn.debug = 4
    imap_conn.authenticate('XOAUTH', lambda x: xoauth_string)
    #imap_conn.list()
    print "Success"
    g = gmailConnector()
    g.M = imap_conn
    g.M.list()
    matched = []
    ml = g.get_mails_from(personfrom)
    print ml #message ids are printed

    result, data = g.M.uid('search', None, "ALL") # search and return uids instead  
    latest_email_uid = data[0].split()[-1]  
    result, data = g.M.uid('fetch', latest_email_uid, '(RFC822)')  
    raw_email = data[0][1] 

    email_message = email.message_from_string(raw_email)  
    print email_message['To']  
    print email.utils.parseaddr(email_message['From']) 

    # this for loop prints the messages that match the search criteria
    for iter in range(len(ml)):
        matched.append(g.get_mail_from_id(ml[iter]) )
        print "From:",personfrom
        print matched[iter]

    tes = g.find_user_subject(searchTag, folder='Inbox')
    print tes
    we = tes[0]
    we2 = g.get_mail_from_id(we)
  
    print we2
    g.M.close()
    g.M.logout()

   #insert this code after callignthis function to return the matched emails that were searched for #
       ##    e_output = []
       ##    e_output = search_Gmail(g,searchTag,personfrom)
   
    return (we2,personfrom)

def search_gmail_body(user,searchtext):

    ''' This function searches a users inbox based on text string '''
    
    import imaplib
    import xoauth
    import email
    import datetime
    import time
    import re
    import urllib2
    import httplib2
    

    
    auth = gmailAuthentication(user)
    access_token = auth[0]
    consumer = auth[2]
   
    xoauth_string = xoauth.GenerateXOauthString(consumer,access_token,user,'imap',xoauth_requestor_id=None, nonce=None, timestamp=None)
  
    imap_hostname = 'imap.googlemail.com'
    imap_conn = imaplib.IMAP4_SSL(imap_hostname)
    imap_conn.debug = 4
    imap_conn.authenticate('XOAUTH', lambda x: xoauth_string)
    #imap_conn.list()
    print "Success"
    g = gmailConnector()
    g.M = imap_conn
    g.M.list()
    matched = []
    ml = g.get_mailboxes()

    matched = []
   
    date = (datetime.datetime.now() - datetime.timedelta(minutes=90)).strftime(r'%d-%b-%Y-%H:%M:%S:%z') 
    print date

    g.M.select('INBOX', readonly=True)
  
    typ, data = g.M.search(None,'(UNSEEN BODY "%s" SENTSINCE {date})'.format(date=date) % searchtext)

    for num in data[0].split():
      typ, data = g.M.fetch(num,'(RFC822)')
      msg = email.message_from_string(data[0][1])
      matched.append(msg)
      
    g.M.logout()
    for iter in range(len(matched)):
        print matched[iter]

    if matched: 
      return matched  #Please note this is a dictionary and can be passed to the sendGmail function to forward the email so iterate through the list to display the matched emails
    else:
      print "No emails matched your criteria"

  
def gmailAuthentication(user):
  # remember to put access_token into the arguments
    import imaplib
    import xoauth
    import email

    consumer = xoauth.OAuthEntity('anonymous', 'anonymous')
    user = 'mrmekit@gmail.com'
    class acc(object):
      """Represents consumer key and secret in OAuth for the user inthe database."""
      __slots__ = ('key','secret')
      def __init__(self, *args, **kwargs):
            self.key = '1/228PQ_FbZHp8euXejUKAn_TqI6wM38I2AL628CsjpLU' # this should be read from the database
            self.secret = '_pl4nGKJ33INjDJras1FNn1k' # this should be read from the database

    access_token = acc() # this sends the access token object created as a dictionary object
         
    xoauth_string = xoauth.GenerateXOauthString(consumer,access_token,user,'imap',xoauth_requestor_id=None, nonce=None, timestamp=None)
  
    imap_hostname = 'imap.googlemail.com'
    imap_conn = imaplib.IMAP4_SSL(imap_hostname)
    imap_conn.debug = 4
    imap_conn.authenticate('XOAUTH', lambda x: xoauth_string)
    #imap_conn.list()

    
    return (access_token,imap_conn, consumer, user)
  

if __name__=="__main__":
    #sendGmail('mrmekit@gmail.com','waldin@gmail.com','hi','Python course2')
    #gmailAuthentication('mrmekit@gmail.com')
    #search_gmail_subject_personfrom('mrmekit@gmail.com', 'IGERT','waldin@gmail.com')
    search_gmail_body('mrmekit@gmail.com','Python')
