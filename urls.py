from django.conf import settings
from django.conf.urls.defaults import *
from django.conf.urls import *
from django.views.generic.simple import direct_to_template
from sign_in_with_google.google import views 
from twitter import views 

from django.contrib import admin
admin.autodiscover()

from pinax.apps.account.openid_consumer import PinaxConsumer


handler500 = "pinax.views.server_error"


urlpatterns = patterns("",
    url(r"^$", direct_to_template, {
        "template": "homepage.html",
    }, name="home"),
    url(r"^admin/invite_user/$", "pinax.apps.signup_codes.views.admin_invite_user", name="admin_invite_user"),
    url(r"^admin/", include(admin.site.urls)),
    url(r"^about/", include("about.urls")),
    url(r"^account/", include("pinax.apps.account.urls")),
    url(r"^openid/", include(PinaxConsumer().urls)),
    url(r"^profiles/", include("idios.urls")),
    url(r"^notices/", include("notification.urls")),
    url(r"^announcements/", include("announcements.urls")),
    url(r"^create/trigger/[A-Za-z]{5,8}/google/$",'sign_in_with_google.google.views.google_login', name="google_login"),
    url(r"^create/trigger/Gmail/google/callback/$",'sign_in_with_google.google.views.google_callback', name="google_callback"),
    url(r"^create/trigger/Gmail/google/callback2/$",'sign_in_with_google.google.views.google_callback2', name="google_callback2"),
    url(r"^mix/create/auth/twitter/$",'sign_in_with_google.twitter.views.twitter_login', name="twitter_login"),
    url(r"^twitter/callback/$",'sign_in_with_google.twitter.views.twitter_callback', name="twitter_callback"),
    url(r"^mix/create/step(1|4)/$","mash.views.channels"),
    url(r"^mix/create/step(2|5)/$","mash.views.services"),
    url(r"^mix/create/auth/$","mash.views.activate"),
    url(r"^mix/create/step(3|6)/$","mash.views.service_criteria"),
    url(r"^mix/create/overview/$","mash.views.complete"),
    url(r"^tasks/", include("tasks.urls")),
    
)


if settings.SERVE_MEDIA:
    urlpatterns += patterns("",
        url(r"", include("staticfiles.urls")),
    )
